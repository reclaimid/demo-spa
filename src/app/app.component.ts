import { Component } from '@angular/core';
import { NullValidationHandler,OAuthService } from 'angular-oauth2-oidc';
import { oauthCodeFlowConfig } from './oauth.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reclaim-demo-spa';

  constructor (private oauthService: OAuthService) {
    this.oauthService.configure(oauthCodeFlowConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.customQueryParams = {
      'claims': '{ "userinfo": { \
                     "preferred_flavor" : {\
                       "essential": true\
                     },\
                     "is_phdstudent" : {\
                       "essential": false\
                     }\
                   }\
                 }'
    }
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  login() {
    this.oauthService.initCodeFlow();
  }
  logout() {
    this.oauthService.logOut();
  }
  getUsername() {
    if (undefined !== this.oauthService.getIdentityClaims()["nickname"])
    {
      return this.oauthService.getIdentityClaims()["nickname"];
    }
    return this.oauthService.getIdentityClaims()["sub"];
  }

  isLoggedIn() {
    return this.oauthService.hasValidIdToken();
  }
}
