# ReclaimDemoSpa

This is a login demo for re:claimID.
It uses a public OpenID Connect client without client credentials and
a [PKCE authorization code flow](https://tools.ietf.org/html/rfc7636) through the [angular-oauth2-oidc](https://manfredsteyer.github.io/angular-oauth2-oidc/docs/) plugin.

## Dependencies

- [gnunet](https://gnunet.org) >= 0.13.2
- [re:claimID WebExtension](https://addons.mozilla.org/firefox/addon/reclaimid/)
- yarn/npm

## Client setup

First, create an identity for the website client:

```
$ gnunet-identity -C democlient
$ MYKEY=$(gnunet-identity -d | grep democlient | awk '{print $3}')
```

Alternatively, you can also manually copy the public key string from
`gnunet-identity -d`.

Then, open `src/app/oauth.config.ts` and replace the `client_id` with
the public key string:

```
...
// The SPA's id. The SPA is registerd with this id at the auth-server
// clientId: 'server.code',
clientId: $MYKEY, //Replace $MYKEY with your client key!!
...
```

Finally, setup the redirect URI and client name:

```
$ gnunet-namestore -z democlient -a -n @ -t RECLAIM_OIDC_REDIRECT -V http://localhost:4200 -e 1d -p
$ gnunet-namestore -z democlient -a -n @ -t RECLAIM_OIDC_CLIENT -V "My re:claimID SPA Demo" -e 1d -p
```

## Build and start

```
$ yarn install
$ yarn build
$ yarn start
```

Open http://localhost:4200 and you should be able to log in using re:claimID.

## Deployment

If you want to use this template on an actual website, remember to set
the CORS setting for the GNUnet REST API (and ideally selectively expose the
OIDC API endpoints through your web server):

```
$ gnunet-config -s rest -o REST_ALLOW_ORIGIN -V <your domain>
```
